A simple Rust implementation of a [dot plot][1] maker used to examine sequence alignment.
The output is sent to stdout as x, y points, which can be used with gnuplot like so:

`./dotplot s1.txt s2.txt | gnuplot -p -e "plot '<cat' with points"`

producing something like this:

![][2]

To install, either clone the repository and then

`cargo build --release`

or

`cargo install dotplot --root dir` to install the binary into dir.

[1]: https://en.wikipedia.org/wiki/Dot_plot_(bioinformatics)
[2]: http://i.imgur.com/JboyEzw.png